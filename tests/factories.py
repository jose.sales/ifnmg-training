
import factory

from training import db
from training.models import Author


LOCALE = 'pt-br'


class AuthorFactory(factory.alchemy.SQLAlchemyModelFactory):
    name = factory.Faker('first_name', locale=LOCALE)

    class Meta:
        model = Author
        sqlalchemy_session = db.session
        sqlalchemy_session_persistence = 'commit'
