import pytest
from training import create_app, db
from pytest_factoryboy import register
from .factories import AuthorFactory


@pytest.fixture
def app():
    app = create_app(environment='Testing')
    with app.app_context():
        db.create_all()
    return app


register(AuthorFactory)
