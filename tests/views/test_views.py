import datetime
from training.models import Author


def test_create_view(app, client):
    payload = {
        'name': 'Jotage Sales'
    }
    response = client.post('/api/v1/author', json=payload)
    data = response.json
    created_at = data.pop('created_at')
    updated_at = data.pop('updated_at')

    author = Author.query.first()
    expected = {
        "id": author.id,
        "name": author.name
    }

    assert response.status_code == 201
    assert data == expected
    assert created_at
    assert updated_at


def test_create_author_without_name(app, client):
    payload = {
        'name': None
    }
    response = client.post('/api/v1/author', json=payload)
    data = response.json
    expected = {
        "errors": {
            "name": [
                "Field may not be null."
            ]
        }
    }

    assert response.status_code == 400
    assert data == expected


def test_list_authors(app, client, author):
    response = client.get('/api/v1/author')
    data = response.json[0]

    created_at = data.pop('created_at')
    updated_at = data.pop('updated_at')

    expected = {
        'id': author.id,
        'name': author.name,
    }

    assert response.status_code == 200
    assert created_at
    assert updated_at
    assert data == expected


def test_deleted_authors(app, client, author):
    author.deleted_at = datetime.datetime.now()
    author.save()

    response = client.get('/api/v1/author')

    assert response.status_code == 200
    assert len(response.json) == 0
