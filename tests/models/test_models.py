import pytest
from training.models import Author
from sqlalchemy.exc import IntegrityError


def test_create_author(app):
    author = Author(name='jotage sales')
    author.save()
    assert author.id


def test_author_without_name(app):
    author = Author(name=None)
    with pytest.raises(IntegrityError):
        author.save()
