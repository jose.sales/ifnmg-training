from training import ma
from training.models import Author


class AuthorSchema(ma.Schema):

    class Meta:
        model = Author
        fields = ('id', 'name', 'created_at', 'updated_at')
