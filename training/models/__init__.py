from .model import Book, Author

__all__ = ('Book', 'Author',)
