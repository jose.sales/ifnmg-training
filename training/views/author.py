import datetime

from flask import jsonify
from flask.views import MethodView
from webargs.flaskparser import use_kwargs

from training.models import Author
from training.schemas.author import AuthorSchema


class AuthorView(MethodView):
    schema = AuthorSchema()

    def get(self):
        authors = Author.query.filter_by(deleted_at=None).all()
        data = self.schema.dump(authors, many=True)
        return jsonify(data)

    @use_kwargs(schema.fields)
    def post(self, **kwargs):
        author = Author(**kwargs)
        author.save()
        data = self.schema.dump(author)
        return jsonify(data), 201

    def delete(self, pk, **kwargs):
        author = Author.query.filter_by(id=pk, deleted_at=None).first_or_404(description='author not found')
        author.deleted_at = datetime.datetime.utcnow()
        author.save()
        return jsonify({}), 204


author = AuthorView.as_view('author')
